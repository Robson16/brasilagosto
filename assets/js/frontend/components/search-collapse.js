export default class SearchCollapse {
	constructor() {
		this.searchTogglers = document.querySelectorAll(".search-toggler");

		this.events()
	}

	// Events Triggers
	events() {
		this.handleTogglers()
	}

	// Methods
	handleTogglers() {
		if (this.searchTogglers) {
			let togglers = Array.from(this.searchTogglers);

			togglers.forEach((toggler) => {
				toggler.addEventListener("click", function (event) {
					let target = this.dataset.target;
					document.querySelector(target).classList.toggle("show");
				});
			});
		}
	}
}
