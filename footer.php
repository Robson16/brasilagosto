<?php

/**
 * The template for displaying the footer
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 */

?>

<footer class="footer">
	<div class="footer-widgets">
		<div class="container">
			<?php if (is_active_sidebar('brasilagosto-sidebar-footer-1')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('brasilagosto-sidebar-footer-1'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('brasilagosto-sidebar-footer-2')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('brasilagosto-sidebar-footer-2'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('brasilagosto-sidebar-footer-3')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('brasilagosto-sidebar-footer-3'); ?>
				</div>
			<?php endif; ?>
			<?php if (is_active_sidebar('brasilagosto-sidebar-footer-4')) : ?>
				<div class="widget-column">
					<?php dynamic_sidebar('brasilagosto-sidebar-footer-4'); ?>
				</div>
			<?php endif; ?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-widgets -->

	<div class="footer-copyright">
		<div class="container">
			<span class="copyright">
				&copy;&nbsp;<?php echo wp_date('Y'); ?>&nbsp;<?php echo bloginfo('title'); ?>&nbsp;&#8211;&nbsp;<?php _e('Developed by', 'brasilagosto') ?>
				&nbsp;
				<a href="https://www.vollup.com/" target="_blank" rel="noopener">
					<img src="<?php echo get_template_directory_uri() . '/assets/images/vollup.png'; ?>" alt="Vollup">
				</a>
			</span>

			<?php
			wp_nav_menu(array(
				'theme_location' => 'footer_menu',
				'depth' => 1,
				'container_class' => 'footer-menu-wrap',
				'menu_class' => 'footer-menu',
			));
			?>
		</div>
		<!-- /.container -->
	</div>
	<!-- /.footer-copyright -->
</footer>

<?php wp_footer(); ?>

</body>

</html>
