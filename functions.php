<?php

/**
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

// Front-End
function brasilagosto_scripts()
{
	// CSS
	wp_enqueue_style('brasilagosto-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('brasilagosto-shared-styles', get_template_directory_uri() . '/assets/css/shared/shared-styles.css', array(), wp_get_theme()->get('Version'));
	wp_enqueue_style('brasilagosto-frontend-styles', get_template_directory_uri() . '/assets/css/frontend/frontend-styles.css', array(), wp_get_theme()->get('Version'));

	// Js
	wp_enqueue_script('comment-reply');
	wp_enqueue_script('brasilagosto-frontend-script', get_template_directory_uri() . '/assets/js/frontend/frontend-bundled.js', array(), wp_get_theme()->get('Version'), true);
}
add_action('wp_enqueue_scripts', 'brasilagosto_scripts');

// Admin Panel
function brasilagosto_admin_scripts()
{
	// CSS
	wp_enqueue_style('brasilagosto_admin', get_template_directory_uri() . '/assets/css/admin/admin-styles.css');

	// Js
	wp_enqueue_script('brasilagosto_taxonomy_thumbnail', get_template_directory_uri() . '/assets/js/admin/taxonomy-thumbnail.js', array('jquery'), '1.0', true);

	// Pass PHP infos to the Javascript Code
	wp_localize_script(
		'brasilagosto_taxonomy_thumbnail',
		'brasilagosto_taxonomy_thumbnail_obj',
		array(
			'version' => sanitize_text_field(esc_html(get_bloginfo("version")))
		)
	);
}
add_action('admin_enqueue_scripts', 'brasilagosto_admin_scripts');

/**
 * Gutenberg scripts
 * @see https://www.billerickson.net/block-styles-in-gutenberg/
 */
function brasilagosto_gutenberg_scripts()
{
	wp_enqueue_style('brasilagosto-webfonts', get_template_directory_uri() . '/assets/css/shared/webfonts.css', array(), wp_get_theme()->get('Version'));
}
add_action('enqueue_block_editor_assets', 'brasilagosto_gutenberg_scripts');

/**
 * Set theme defaults and register support for various WordPress features.
 */
function brasilagosto_setup()
{
	// Enabling translation support
	$textdomain = 'brasilagosto';
	load_theme_textdomain($textdomain, get_stylesheet_directory() . '/languages/');
	load_theme_textdomain($textdomain, get_template_directory() . '/languages/');

	// Menu registration
	register_nav_menus(array(
		'main_menu' => __('Main Menu', 'brasilagosto'),
		'footer_menu' => __('Footer Menu', 'brasilagosto'),
	));

	// Load custom styles in the editor.
	add_theme_support('editor-styles');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/shared/shared-styles.css');
	add_editor_style(get_stylesheet_directory_uri() . '/assets/css/admin/editor-styles.css');

	// Add Custom Header Support
	add_theme_support('custom-header', array(
		'default-image'      => 'https://via.placeholder.com/1920x430',
		'default-text-color' => 'fff',
		'width'              => 1920,
		'height'             => 430,
		'flex-width'         => true,
		'flex-height'        => true,
	));

	// Let WordPress manage the document title.
	add_theme_support('title-tag');

	// Enable support for featured image on posts and pages.
	add_theme_support('post-thumbnails');

	// Enable support for embedded media for full weight
	add_theme_support('responsive-embeds');

	// Enables wide and full dimensions
	add_theme_support('align-wide');

	// Standard style for each block.
	add_theme_support('wp-block-styles');

	// Creates the specific color palette
	add_theme_support('editor-color-palette', array(
		array(
			'name'  => __('White', 'brasilagosto'),
			'slug'  => 'white',
			'color' => '#ffffff',
		),
		array(
			'name'  => __('Quick Silver', 'brasilagosto'),
			'slug'  => 'quick-silver',
			'color' => '#a3a3a3',
		),
		array(
			'name'  => __('Sonic Silver', 'brasilagosto'),
			'slug'  => 'sonic-silver',
			'color' => '#727475',
		),
		array(
			'name'  => __('Prussian Blue', 'brasilagosto'),
			'slug'  => 'prussian-blue',
			'color' => '#012b3d',
		),
		array(
			'name'  => __('Army Green', 'brasilagosto'),
			'slug'  => 'army-green',
			'color' => '#47512a',
		),
		array(
			'name'  => __('Maize Crayola', 'brasilagosto'),
			'slug'  => 'maize-crayola',
			'color' => '#efc94c',
		),
		array(
			'name'  => __('Goldenrod', 'brasilagosto'),
			'slug'  => 'goldenrod',
			'color' => '#e2a831',
		),
		array(
			'name'  => __('Satin Sheen Gold', 'brasilagosto'),
			'slug'  => 'satin-sheen-gold',
			'color' => '#d0a53e',
		),
		array(
			'name'  => __('Harvest Gold', 'brasilagosto'),
			'slug'  => 'harvest-gold',
			'color' => '#dd9933',
		),
		array(
			'name'  => __('Ochre', 'brasilagosto'),
			'slug'  => 'ochre',
			'color' => '#d57824',
		),
		array(
			'name'  => __('Fire Opal', 'brasilagosto'),
			'slug'  => 'fire-opal',
			'color' => '#e66962',
		),
		array(
			'name'  => __('Cedar Chest', 'brasilagosto'),
			'slug'  => 'cedar-chest',
			'color' => '#d55342',
		),
		array(
			'name'  => __('Flame', 'brasilagosto'),
			'slug'  => 'flame',
			'color' => '#db522c',
		),
		array(
			'name'  => __('Sinopia', 'brasilagosto'),
			'slug'  => 'sinopia',
			'color' => '#be5231',
		),
		array(
			'name'  => __('Liver Organ', 'brasilagosto'),
			'slug'  => 'liver-organ',
			'color' => '#622312',
		),
		array(
			'name'  => __('Black Bean', 'brasilagosto'),
			'slug'  => 'black-bean',
			'color' => '#480c00',
		),
		array(
			'name'  => __('Black', 'brasilagosto'),
			'slug'  => 'black',
			'color' => '#000000',
		),
	));

	// Custom font sizes.
	add_theme_support('editor-font-sizes', array(
		array(
			'name' => __('Small', 'brasilagosto'),
			'size' => 16,
			'slug' => 'small',
		),
		array(
			'name' => __('Normal', 'brasilagosto'),
			'size' => 18,
			'slug' => 'normal',
		),
		array(
			'name' => __('Medium', 'brasilagosto'),
			'size' => 24,
			'slug' => 'medium',
		),
		array(
			'name' => __('Big', 'brasilagosto'),
			'size' => 40,
			'slug' => 'big',
		),
		array(
			'name' => __('Huge', 'brasilagosto'),
			'size' => 50,
			'slug' => 'huge',
		),
	));

	// Custom image sizes
	add_image_size('thumbnail_excerpt', 410, 260, true);

	/**
	 * Custom blocks styles.
	 *
	 * @see https://wpblockz.com/tutorial/register-block-styles-in-wordpress/
	 * @link https://developer.wordpress.org/block-editor/reference-guides/filters/block-filters/
	 */
	register_block_style('core/image', array(
		'name' => 'internal-caption',
		'label' => __('Internal caption', 'brasilagosto'),
	));

	register_block_style('core/gallery', array(
		'name' => 'internal-caption',
		'label' => __('Internal caption', 'brasilagosto'),
	));
}
add_action('after_setup_theme', 'brasilagosto_setup');

/**
 * Registration of widget areas.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function brasilagosto_sidebars()
{
	// Args used in all calls register_sidebar().
	$shared_args = array(
		'before_title' => '<h4 class="widget-title">',
		'after_title' => '</h4>',
		'before_widget' => '<div class="widget %2$s"><div class="widget-content">',
		'after_widget' => '</div></div>',
	);

	// Footer #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #1', 'brasilagosto'),
		'id' => 'brasilagosto-sidebar-footer-1',
		'description' => __('The widgets in this area will be displayed in the first column in Footer.', 'brasilagosto'),
	)));

	// Footer #2
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #2', 'brasilagosto'),
		'id' => 'brasilagosto-sidebar-footer-2',
		'description' => __('The widgets in this area will be displayed in the second column in Footer.', 'brasilagosto'),
	)));

	// Footer #3
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #3', 'brasilagosto'),
		'id' => 'brasilagosto-sidebar-footer-3',
		'description' => __('The widgets in this area will be displayed in the third column in Footer.', 'brasilagosto'),
	)));

	// Footer #4
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Footer #4', 'brasilagosto'),
		'id' => 'brasilagosto-sidebar-footer-4',
		'description' => __('The widgets in this area will be displayed in the fourth column in Footer.', 'brasilagosto'),
	)));


	// Barra Lateral Blog #1
	register_sidebar(array_merge($shared_args, array(
		'name' => __('Sidebar Blog', 'brasilagosto'),
		'id' => 'brasilagosto-sidebar-blog',
		'description' => __('The widgets in this area will be displayed in the blog sidebar.', 'brasilagosto'),
	)));
}
add_action('widgets_init', 'brasilagosto_sidebars');

/**
 * Remove website field from comment form
 *
 */
function brasilagosto_website_remove($fields)
{
	if (isset($fields['url']))
		unset($fields['url']);
	return $fields;
}
add_filter('comment_form_default_fields', 'brasilagosto_website_remove');

/**
 *  TGM Plugin
 */
require_once get_template_directory() . '/includes/required-plugins.php';

/**
 *  Kirki Framework Config
 */
require_once get_template_directory() . '/includes/kirki/kirki-config.php';

/**
 *  WordPress Bootstrap Nav Walker
 */
require_once get_template_directory() . '/includes/classes/class-wp-bootstrap-navwalker.php';

/**
 * 	Custom menu item field
 */
require_once get_template_directory() . '/includes/custom-menu-item-field.php';

/**
 *	Custom Taxonomy Thumbnail
 */
require_once get_template_directory() . '/includes/taxonomy-thumbnail.php';

/**
 *	Search Advanced functions
 */
require_once get_template_directory() . '/includes/search-advanced.php';

/**
 * Custom Shortcodes
 */
require_once get_template_directory() . '/includes/shortcodes/recent-posts.php';
