<?php

/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 */
?>
<!doctype html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo('charset'); ?>" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

	<?php wp_body_open(); ?>

	<header id="header">

		<div id="search-collapse" class="searchbar">
			<div class="container">
				<?php get_search_form(); ?>
			</div>
			<!-- /.container -->
		</div>

		<nav id="navbar" class="navbar ">
			<div class="container">
				<?php
				$logo = get_theme_mod('brasilagosto_setting_logo', '');
				$logoAlt = get_theme_mod('brasilagosto_setting_logo_alt', '');

				if ($logo && $logoAlt) :
				?>
					<a class="navbar-brand-link" href="<?php echo esc_url(get_home_url()); ?>">
						<img class="navbar-brand" src="<?php echo esc_url($logo['url']); ?>" width="<?php echo esc_attr($logo['width']); ?>" height="<?php echo esc_attr($logo['height']); ?>" alt="<?php echo get_bloginfo('title'); ?>">
						<img class="navbar-brand alt" src="<?php echo esc_url($logoAlt['url']); ?>" width="<?php echo esc_attr($logo['width']); ?>" height="<?php echo esc_attr($logo['height']); ?>" alt="<?php echo get_bloginfo('title'); ?>">
					</a>
				<?php
				else :
					echo '<h1 style="margin: 0;">' . get_bloginfo('title') . '</h1>';
				endif;
				?>

				<button type="button" class="navbar-toggler" data-target="#navbar-nav">
					<span class="navbar-toggler-icon">
						<div class="bar1"></div>
						<div class="bar2"></div>
						<div class="bar3"></div>
					</span>
				</button>

				<?php
				wp_nav_menu(array(
					'theme_location' => 'main_menu',
					'depth' => 2,
					'container' => 'div',
					'container_class' => 'collapse navbar-collapse',
					'container_id' => 'navbar-nav',
					'menu_class' => 'navbar-nav has-dropdown-menu-to-right',
					'fallback_cb' => 'WP_Bootstrap_Navwalker::fallback',
					'walker' => new WP_Bootstrap_Navwalker()
				));
				?>

				<button type="button" class="search-icon-wrapper search-toggler" data-target="#search-collapse" aria-label="<?php _e('Search', 'brasilagosto'); ?>">
					<div class="search-icon">
						<div class="search-icon__circle"></div>
						<div class="search-icon__rectangle"></div>
					</div>
				</button>
			</div>
			<!-- /.container -->
		</nav>

		<?php if (!is_page()) : ?>
			<div class="header-inner" style="
				background-image: url(
					<?php
					if (is_archive() && brasilagosto_taxonomy_image(get_queried_object()->term_id)) {
						echo brasilagosto_taxonomy_image(get_queried_object()->term_id);
					} elseif (is_single() && has_post_thumbnail()) {
						echo get_the_post_thumbnail_url(get_the_ID(), 'full');
					} else {
						header_image();
					}
					?>
				);
				color: <?php echo '#' . get_header_textcolor(); ?>;
			">
				<div class="container">
					<?php
					if (is_search()) {
						// Check if is search WITHOUT category filter
						if (get_search_query() && !get_query_var('category', FALSE)) {
							// Eg: "Search results for: Fish"
							echo sprintf(
								'<span class="header-title">%s <span>%s</span></span>',
								__('Search results for:', 'brasilagosto'),
								get_search_query()
							);
						}

						// Check if is advanced search WITH category filter, then get the category name by id of query params
						if (get_search_query() && get_query_var('category')) {
							// Eg: "Search results for: Fish, in Recipes"
							echo sprintf(
								'<span class="header-title">%s <span>%s %s %s</span></span>',
								__('Search results for:', 'brasilagosto'),
								get_search_query(),
								__(', in ', 'brasilagosto'),
								get_cat_name(get_query_var('category'))
							);
						}

						// Check if is advanced search ONLY with category filter, then get the category name by id of query params
						if (!get_search_query() && get_query_var('category')) {
							// Eg: "Search results for: Recipes"
							echo sprintf(
								'<span class="header-title">%s <span>%s</span></span>',
								__('Search results for:', 'brasilagosto'),
								get_cat_name(get_query_var('category'))
							);
						}
					}

					if (is_archive()) echo sprintf('<h1 class="header-title">%s</h1>', single_term_title("", false));
					if (is_single()) echo sprintf('<span class="header-title">%s</span>', get_the_title());
					if (is_home()) echo '<h1 class="header-title">Blog</h1>';
					?>
				</div>
				<!-- /.container -->
			</div>
			<!-- /.page-header -->
		<?php endif; ?>
	</header>
