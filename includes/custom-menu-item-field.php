<?php

/**
 * Custom menu item field
 *
 * @link https://felipeelia.com.br/como-criar-novos-campos-nos-itens-de-menu-do-wordpress/
 */


// How to display fields
function brasilagosto_wp_nav_menu_item_custom_fields($item_id, $item, $depth, $args)
{
	// Itens de menu são gravados como posts pelo WP.
	$is_button = (bool) get_post_meta($item_id, 'is_button', true);

	// Tem post e vídeo sobre nonce aqui no blog também.
	wp_nonce_field('nav_menu_edit', 'nav_menu_is_button');
?>
	<div>
		<input type="checkbox" class="nav-menu-is-button" name="nav-menu-is-button[<?php echo $item_id; ?>]" id="nav-menu-is-button-for-<?php echo $item_id; ?>" <?php checked('1', $is_button); ?> value="1">
		<label for="nav-menu-is-button-for-<?php echo $item_id; ?>">
			<?php _e('Display as button', 'brasilagosto'); ?>
		</label>
	</div>
<?php
}
add_action('wp_nav_menu_item_custom_fields', 'brasilagosto_wp_nav_menu_item_custom_fields', 10, 4);


// How to save the field value
function brasilagosto_wp_update_nav_menu_item($menu_id, $menu_item_db_id)
{
	// Verifica o nonce criado na outra função.
	if (!isset($_POST['nav_menu_is_button']) || !wp_verify_nonce($_POST['nav_menu_is_button'], 'nav_menu_edit')) {
		return;
	}

	$is_button = (!empty($_POST['nav-menu-is-button'][$menu_item_db_id]) && '1' === $_POST['nav-menu-is-button'][$menu_item_db_id]);
	update_post_meta($menu_item_db_id, 'is_button', $is_button);
}
add_action('wp_update_nav_menu_item', 'brasilagosto_wp_update_nav_menu_item', 10, 2);


// How to use field value
function my_nav_menu_css_class($classes, $item, $args = array(), $depth = 0)
{
	$is_button = (bool) get_post_meta($item->ID, 'is_button', true);
	if ($is_button) {
		$classes[] = 'button';
	}
	return $classes;
}
add_filter('nav_menu_css_class', 'my_nav_menu_css_class', 10, 4);
