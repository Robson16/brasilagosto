<?php

new \Kirki\Section(
	'brasilagosto_section_logo',
	array(
		'title'          => esc_html__('Logo', 'brasilagosto'),
		'description'    => esc_html__('Your Logo, to use in places like the main navbar', 'brasilagosto'),
		'priority'       => 70,
	)
);

new \Kirki\Field\Image(
	array(
		'type'        => 'image',
		'settings'    => 'brasilagosto_setting_logo',
		'label'       => esc_html__('Main Logo', 'brasilagosto'),
		'description' => esc_html__('Use PNG image with 180px by 38px', 'brasilagosto'),
		'section'     => 'brasilagosto_section_logo',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		)
	)
);

new \Kirki\Field\Image(
	array(
		'type'        => 'image',
		'settings'    => 'brasilagosto_setting_logo_alt',
		'label'       => esc_html__('Main Logo Alt', 'brasilagosto'),
		'description' => esc_html__('Use PNG image with 180px by 38px', 'brasilagosto'),
		'section'     => 'brasilagosto_section_logo',
		'default'     => '',
		'choices'     => array(
			'save_as' => 'array',
		)
	)
);
