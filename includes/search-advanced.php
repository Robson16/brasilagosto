<?php

/**
 * Custom Search bar with categories filter
 *
 * @link https://stevepolito.design/blog/create-a-custom-search-form-in-wordpress/
 */


/**
 * Create Custom Query Vars
 * https://codex.wordpress.org/Function_Reference/get_query_var#Custom_Query_Vars
 */
function brasilagosto_add_query_vars_filter($vars)
{
	// add custom query vars that will be public
	// https://codex.wordpress.org/WordPress_Query_Vars
	$vars[] .= 'category';
	return $vars;
}
add_filter('query_vars', 'brasilagosto_add_query_vars_filter');


/**
 * Override Query
 * https://codex.wordpress.org/Plugin_API/Action_Reference/pre_get_posts
 */
function brasilagosto_main_query($query)
{
	// only run this query if we're  not on the admin side
	if ($query->is_main_query() && !is_admin()) {

		// get query vars from url.
		// https://codex.wordpress.org/Function_Reference/get_query_var#Examples

		// example.com/?category=6
		$category = get_query_var('category', FALSE);

		// final tax_query
		$query->set('category__in', $category);
	}
}
add_action('pre_get_posts', 'brasilagosto_main_query');
