<?php

/**
 *
 */
function brasilagosto_recent_posts($atts)
{
	ob_start();

	// Attributes
	$atts = shortcode_atts(
		array(
			'categories_slugs'	=> '',
			'number_of_items'	=> '6',
		),
		$atts
	);

	$categories_array = explode(',', $atts['categories_slugs']);

	// Iterate array through array_walk() function
	// and use trim() function to remove all
	// whitespaces from every array objects
	array_walk($categories_array, create_function(
		'&$val',
		'$val = trim($val);'
	));

	// $array = array('a', 'a', 'a', 'a', 'b', 'b', 'c');
	// $count = count(array_keys($array, 'a', true));
	// echo '<pre>';
	// echo "Found $count letter a's.";
	// echo '</pre>';

	/**
	 * If function from Polylang plugin is available to use
	 * @link https://polylang.wordpress.com/documentation/documentation-for-developers/functions-reference/
	 */
	if (function_exists('pll_the_languages')) {
		$all_languages = pll_the_languages(array(
			'raw' => 1
		));

		$all_languages_slug = array_column($all_languages, 'slug');
	}

	echo '<div class="recent-posts">';
	// Check if is there specifics slugs to list
	if (!empty($categories_array)) {
		// Retrieve posts for each slug received
		foreach ($categories_array as $category_index => $category_name) {

			// Measure the offset for categories that repeat
			$control_array = array_slice($categories_array, 0, $category_index);
			$offset = count(array_keys($control_array, $category_name, true));

			// Custom quer
			$post_items = new WP_Query(array(
				'post_type'			=> 'post',
				'lang' 				=> ($all_languages_slug) ? implode(',', $all_languages_slug) : '',
				'post_status'		=> 'publish',
				'category_name'		=> $category_name,
				'posts_per_page'	=> 1,
				'offset' 			=> $offset,
			));

			while ($post_items->have_posts()) {
				$post_items->the_post();
				get_template_part('partials/content/content', 'excerpt');
			}
		}
	} else {
		// Custom query
		$post_items = new WP_Query(array(
			'post_type'			=> 'post',
			'post_status'		=> 'publish',
			'posts_per_page'	=> $atts['number_of_items'],
		));

		while ($post_items->have_posts()) {
			$post_items->the_post();
			get_template_part('partials/content/content', 'excerpt');
		}
	}
	echo '</div>';

	// Reset the query postdata
	wp_reset_postdata();

	$recent_posts_html = ob_get_contents();
	ob_end_clean();
	return $recent_posts_html;
}
add_shortcode('brasilagosto-recent-posts', 'brasilagosto_recent_posts');
