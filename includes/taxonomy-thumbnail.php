<?php

// define('brasilagosto_IMAGE_PLACEHOLDER', esc_url(get_template_directory_uri() . "/assets/images/tr.png"));
define('brasilagosto_IMAGE_PLACEHOLDER', esc_url('https://via.placeholder.com/64x64'));

add_action('admin_init', 'brasilagosto_init');
function brasilagosto_init()
{
	$brasilagosto_taxonomies = get_taxonomies();
	if (is_array($brasilagosto_taxonomies)) {
		$brasilagosto__options = get_option('brasilagosto_options');
		if (empty($brasilagosto__options['excluded_taxonomies'])) {
			$brasilagosto__options['excluded_taxonomies'] = [];
		}

		foreach ($brasilagosto_taxonomies as $brasilagosto_taxonomy) {
			if (in_array($brasilagosto_taxonomy, $brasilagosto__options['excluded_taxonomies'])) {
				continue;
			}
			add_action($brasilagosto_taxonomy . '_add_form_fields', 'brasilagosto_add_texonomy_field');
			add_action($brasilagosto_taxonomy . '_edit_form_fields', 'brasilagosto_edit_texonomy_field');
			add_filter('manage_edit-' . $brasilagosto_taxonomy . '_columns', 'brasilagosto_taxonomy_columns');
			add_filter('manage_' . $brasilagosto_taxonomy . '_custom_column', 'brasilagosto_taxonomy_column', 10, 3);
		}
	}
}

// add image field in add form
function brasilagosto_add_texonomy_field()
{
	if (get_bloginfo('version') >= 3.5) {
		wp_enqueue_media();
	} else {
		wp_enqueue_style('thickbox');
		wp_enqueue_script('thickbox');
	}

	echo '<div class="form-field">
		<label for="taxonomy_image">' .
		esc_html__('Image', 'brasilagosto') .
		'</label>
		<input type="text" name="taxonomy_image" id="taxonomy_image" value="" />
		<br/>
		<button class="brasilagosto_upload_image_button button">' .
		esc_html__('Upload/Add image', 'brasilagosto') .
		'</button>
	</div>';
}

// add image field in edit form
function brasilagosto_edit_texonomy_field($taxonomy)
{
	if (get_bloginfo('version') >= 3.5) {
		wp_enqueue_media();
	} else {
		wp_enqueue_style('thickbox');
		wp_enqueue_script('thickbox');
	}

	if (brasilagosto_taxonomy_image_url($taxonomy->term_id, null, true) == brasilagosto_IMAGE_PLACEHOLDER) {
		$image_url = "";
	} else {
		$image_url = brasilagosto_taxonomy_image_url($taxonomy->term_id, null, true);
	}
	echo '<tr class="form-field">
		<th scope="row" valign="top"><label for="taxonomy_image">' .
		esc_html__('Image', 'brasilagosto') .
		'</label></th>
		<td><img class="taxonomy-image" src="' .
		esc_url(brasilagosto_taxonomy_image_url($taxonomy->term_id, 'medium', true)) .
		'"/><br/>
		<input type="text" name="taxonomy_image" id="taxonomy_image" value="' .
		esc_url($image_url) .
		'" /><br />
		<button class="brasilagosto_upload_image_button button">' .
		esc_html__('Upload/Add image', 'brasilagosto') .
		'</button>
		<button class="brasilagosto_remove_image_button button">' .
		esc_html__('Remove image', 'brasilagosto') .
		'</button>
		</td>
	</tr>';
}

// save our taxonomy image while edit or save term
add_action('edit_term', 'brasilagosto_save_taxonomy_image');
add_action('create_term', 'brasilagosto_save_taxonomy_image');
function brasilagosto_save_taxonomy_image($term_id)
{
	if (isset($_POST['taxonomy_image'])) {
		update_option('brasilagosto_taxonomy_image' . $term_id, sanitize_text_field($_POST['taxonomy_image']), null);
	}
}

// get attachment ID by image url
function brasilagosto_get_attachment_id_by_url($image_src)
{
	global $wpdb;
	$id = $wpdb->get_var($wpdb->prepare("SELECT ID FROM $wpdb->posts WHERE guid = %s", $image_src));
	return !empty($id) ? $id : null;
}

// get taxonomy image url for the given term_id (Place holder image by default)
function brasilagosto_taxonomy_image_url($term_id = null, $size = 'full', $return_placeholder = false)
{
	if (!$term_id) {
		if (is_category()) {
			$term_id = get_query_var('cat');
		} elseif (is_tag()) {
			$term_id = get_query_var('tag_id');
		} elseif (is_tax()) {
			$current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
			$term_id = $current_term->term_id;
		}
	}

	$taxonomy_image_url = get_option('brasilagosto_taxonomy_image' . $term_id);
	if (!empty($taxonomy_image_url)) {
		$attachment_id = brasilagosto_get_attachment_id_by_url($taxonomy_image_url);
		if (!empty($attachment_id)) {
			$taxonomy_image_url = wp_get_attachment_image_src($attachment_id, $size);
			$taxonomy_image_url = $taxonomy_image_url[0];
		}
	}

	if ($return_placeholder) {
		return $taxonomy_image_url != '' ? $taxonomy_image_url : brasilagosto_IMAGE_PLACEHOLDER;
	} else {
		return $taxonomy_image_url;
	}
}

function brasilagosto_quick_edit_custom_box($column_name, $screen, $name)
{
	if ($column_name == 'thumb') {
		echo '<fieldset>
		<div class="thumb inline-edit-col">
			<label>
				<span class="title"><img src="" alt=""/></span>
				<span class="input-text-wrap"><input type="text" name="taxonomy_image" value="" class="tax_list" /></span>
				<span class="input-text-wrap">
					<button class="brasilagosto_upload_image_button button">' .
			esc_html__('Upload/Add image', 'brasilagosto') .
			'</button>
					<button class="brasilagosto_remove_image_button button">' .
			esc_html__('Remove image', 'brasilagosto') .
			'</button>
				</span>
			</label>
		</div>
	</fieldset>';
	}
}

/**
 * Thumbnail column added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @return void
 */
function brasilagosto_taxonomy_columns($columns)
{
	$new_columns = [];
	$new_columns['cb'] = $columns['cb'];
	$new_columns['thumb'] = esc_html__('Image', 'brasilagosto');

	unset($columns['cb']);

	return array_merge($new_columns, $columns);
}

/**
 * Thumbnail column value added to category admin.
 *
 * @access public
 * @param mixed $columns
 * @param mixed $column
 * @param mixed $id
 * @return void
 */
function brasilagosto_taxonomy_column($columns, $column, $id)
{
	if ($column == 'thumb') {
		$columns = '<span><img width="100px" src="' . esc_url(brasilagosto_taxonomy_image_url($id, 'thumbnail', true)) . '" alt="' . esc_html__('Thumbnail', 'brasilagosto') . '" class="wp-post-image" /></span>';
	}

	return $columns;
}

// Change 'insert into post' to 'use this image'
function brasilagosto_change_insert_button_text($safe_text, $text)
{
	return str_replace("Insert into Post", "Use this image", $text);
}

// Style the image in category list
if (strpos($_SERVER['SCRIPT_NAME'], 'edit-tags.php') > 0) {
	add_action('quick_edit_custom_box', 'brasilagosto_quick_edit_custom_box', 10, 3);
	add_filter("attribute_escape", "brasilagosto_change_insert_button_text", 10, 2);
}

// Settings section description
function brasilagosto_section_text()
{
	echo '<p>' . esc_html__('Please select the taxonomies you want to exclude it from Categories Images plugin', 'brasilagosto') . '</p>';
}

// Excluded taxonomies checkboxs
function brasilagosto_excluded_taxonomies()
{
	$options = get_option('brasilagosto_options');
	$disabled_taxonomies = ['nav_menu', 'link_category', 'post_format'];
	foreach (get_taxonomies() as $tax) :
		if (in_array($tax, $disabled_taxonomies)) {
			continue;
		} ?>
		<input type="checkbox" name="brasilagosto_options[excluded_taxonomies][<?php echo esc_html($tax); ?>]" value="<?php echo esc_html($tax); ?>" <?php checked(isset($options['excluded_taxonomies'][$tax])); ?> /> <?php echo esc_html($tax); ?>
		<br />
	<?php
	endforeach;
}

// Validating options
function brasilagosto_options_validate($input)
{
	return $input;
}

// Plugin option page
function brasilagosto_options()
{
	if (!current_user_can('manage_options')) {
		wp_die(esc_html__('You do not have sufficient permissions to access this page.', 'brasilagosto'));
	}
	$options = get_option('brasilagosto_options');
	?>
	<div class="wrap">
		<h2><?php esc_html_e('Categories Images', 'brasilagosto'); ?></h2>
		<form method="post" action="options.php">
			<?php settings_fields('brasilagosto_options'); ?>
			<?php do_settings_sections('zci-options'); ?>
			<?php submit_button(); ?>
		</form>
	</div>
<?php
}

// display taxonomy image for the given term_id
function brasilagosto_taxonomy_image($term_id = null, $size = 'full', $attr = null, $echo = false)
{
	if (!$term_id) {
		if (is_category()) {
			$term_id = get_query_var('cat');
		} elseif (is_tag()) {
			$term_id = get_query_var('tag_id');
		} elseif (is_tax()) {
			$current_term = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
			$term_id = $current_term->term_id;
		}
	}

	$taxonomy_image_url = get_option('brasilagosto_taxonomy_image' . $term_id);
	if (!empty($taxonomy_image_url)) {
		$attachment_id = brasilagosto_get_attachment_id_by_url($taxonomy_image_url);
		if (!empty($attachment_id)) {
			$taxonomy_image = wp_get_attachment_image($attachment_id, $size, false, $attr);
		}
	}

	if ($echo) {
		echo esc_url($taxonomy_image_url);
	} else {
		return esc_url($taxonomy_image_url);
	}
}
