<?php

/**
 * Template for displaying search forms
 *
 * @link https://polylang.pro/always-use-get_search_form-to-create-search-forms/
 */
?>

<form role="search" method="get" class="search-form" action="<?php echo esc_url(home_url('/')); ?>">
	<!-- gather data to use in form fields  -->
	<?php
	$categories = get_categories();
	?>

	<label class="search-label" for="s"><?php _e('Search', 'brasilagosto'); ?></label>
	<input type="search" id="s" name="s" class="search-field" placeholder="<?php _e('Search for', 'brasilagosto') ?>" value="<?php echo get_search_query(); ?>" aria-label="<?php _e('Search for', 'brasilagosto'); ?>" />

	<?php if (!empty($categories)) { ?>

		<div class="select-wrapper">
			<label class="search-label" for="search-filter"><?php _e('Category Filter', 'brasilagosto'); ?></label>
			<select id="search-filter" class="search-filter" name="category">

				<option disabled selected value>
					<?php _e('Filter by category', 'brasilagosto'); ?>
				</option>

				<?php foreach ($categories as &$category) { ?>
					<option value="<?php echo $category->term_id; ?>">
						<?php echo $category->name; ?>
					</option>
				<?php } ?>
			</select>
		</div>
		<!-- /.select-wrapper -->

	<?php } ?>

	<button class="search-submit" type="submit"><?php _e('Search!', 'brasilagosto'); ?></button>

</form>
