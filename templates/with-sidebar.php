<?php
/*
Template Name: With Sidebar
Template Post Type: post, page
*/

get_header();
?>

<main>
	<div class="container">
		<section>
			<?php
			while (have_posts()) {
				the_post();
				get_template_part('partials/content/content', get_post_format());
			}

			if (comments_open() || get_comments_number()) comments_template();

			get_template_part('partials/content/content', 'related');
			?>
		</section>

		<?php get_sidebar(); ?>
	</div>
	<!-- /.container -->
</main>

<?php
get_footer();
